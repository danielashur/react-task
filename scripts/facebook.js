import React, { Component } from 'react';
import Input from './input';
import InputNumber from './input-number';

export default class Facebook extends Component {


  render() {
    return (<div>
      <div className="container1">
        <h3>Facebook Social Tags(Open Graph)</h3>
        <p className="p"><a href="">Read more </a>or use <a href="">Facebook Sharing Debugger</a></p>
        <p className="p">What is the name of your site?</p>
        <Input placeHolder={'Name of your site'} />
        <p className="p">A title describing your spark:</p>
        <Input placeHolder={'Your awesome title'} />
        <p className="p">Describe what your spark os about (try to use keywords here):</p>
        <Input placeHolder={'Compelling description'} />
        <p className="p">Type <a href="">(explanation)</a></p>
        <InputNumber className="inputNumber" />
        <p className="p">Did you create a vanity URL for this spark?</p>
        <Input placeHolder={'Vanity URL'} />
        <p className="p">An URL to your image:</p>
        <Input placeHolder={'Preview image'} />
        <p className="p">Facebook App ID (required for <a href="">Facebook Insights</a>):</p>
        <Input placeHolder={'Facebook App ID'} />
      </div>
    </div>);
  }
}

