import React, { Component } from 'react';
import Input from './input';

export default class Google extends Component {


  render() {
    return (<div>
      <div className="container">
        <h3>Google Tag Manager</h3>
        <Input placeHolder={'GTM-1234567'} />
        <p>(Google Tag Manager will only work on Preview or Publish versions)</p>
      </div>
    </div>);
  }
}

