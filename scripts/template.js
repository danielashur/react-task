import React, { Component } from 'react';
import Facebook from './facebook';
import Google from './google'
import Tab from './tab';
import Redirect from './redirect';
export default class Template extends Component {


  render() {
    return (<div className="main">
      <Facebook/>
      <Tab/>
      <Google/>
      <Redirect/>
    </div>);
  }
}

