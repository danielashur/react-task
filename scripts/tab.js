import React, { Component } from 'react';
import Input from './input';

export default class Tab extends Component {


  render() {
    return (<div>
      <div className="container">
        <h3>Tab title</h3>
        <p>Choose the text on the browser tab`s label: </p>
        <Input placeHolder={"Browser tab`s title..."} />
      </div>
    </div>);
  }
}

