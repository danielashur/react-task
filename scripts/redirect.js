import React, { Component } from 'react';
import Input from './input';
import InputNumber from './input-number';

export default class Redirect extends Component {


  render() {
    return (<div>
      <div className="container">
        <h3>Redirect Desktop Traffic</h3>
        <p>Visitors who use a desktop computer will be redirected to the following URL:</p>
        <InputNumber className="inputNumber1"/>
        <Input placeHolder={"yoursite.com/path/to/page.html"}/>
        <p>(Redirection will only work on Preview or Publish versions)</p>
      </div>
    </div>);
  }
}

