import React from 'react';

export default class InputNumber extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.inputValue !== this.state.inputValue;
  }


  myOnChange(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    return (
      <input
        type="number"
        onChange={e => this.myOnChange(e)}
        min={this.props.min}
        max={this.props.max}
        className={this.props.className}
        value={this.state.inputValue}
      />
    );
  }
}
