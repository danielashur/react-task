import React from 'react';

export default class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      placeHolder: props.placeHolder || 'Type Here'
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.inputValue !== this.state.inputValue;
  }



  myOnChange(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    return (
      <input type='text'
             onChange={e => this.myOnChange(e)}
             value={this.state.inputValue}
             placeholder={this.state.placeHolder}/>
    );
  }
}
